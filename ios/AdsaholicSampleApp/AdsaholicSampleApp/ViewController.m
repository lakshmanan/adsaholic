//
//  ViewController.m
//  AppsaholicSDKTesting
//
//  Created by abhijeet upadhyay on 30/12/14.
//  Copyright (c) 2014 Perk. All rights reserved.
//

#import "ViewController.h"

#import "AppsaholicSDK.h"
#import "AppsaholicAds.h"

#import <Adsaholic/Adsaholic.h>

NSString * const API_KEY = @"f54f0b0fff9a9430e65a3b6af6a95fe6e74b81ab";
NSString * const ONE_TAP =@"ae9ab0a49eb6244dc4ed8b93422d7bbd043436b7";
NSString * const PLACEMENT_ID = @"4cff9febb0497a49a45a6182bbb71337b3137675";

@interface ViewController ()<AppsaholicAdServerProtocol>
{
    UITapGestureRecognizer *singleTap;
    UIView *awardView;
    UIView *notificationView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"AppsaholicSDK Sample";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    ((AppsaholicSDK*)[AppsaholicSDK sharedManager]).appsaholic_rootViewController = self;
    ((AdsaholicSDK*)[AdsaholicSDK sharedManager]).appRootViewController = self;
    
    [[AppsaholicSDK sharedManager] startSession:API_KEY withSuccess:^(BOOL success, NSString* status) {
        self.view.userInteractionEnabled = YES;
        [[AppsaholicSDK sharedManager] getUserInformation:^(BOOL success, NSDictionary *info) {
            NSLog(@"user info = %@",info);
        }];
    }];
    ((AppsaholicAds*)[AppsaholicAds sharedManager]).adServerDelegate = self;
}


- (IBAction)openPortal:(id)sender
{
    [[AppsaholicSDK sharedManager] showPortal];
}

- (IBAction)login:(id)sender
{
    [[AppsaholicSDK sharedManager] loginCall:self];
}

- (IBAction)oneTap:(id)sender {
    BOOL custom = NO;
    
    NSString *tapOption = [[NSUserDefaults standardUserDefaults] stringForKey:@"tapOne"] ;
    if (tapOption == nil){
        tapOption = @"0";
    }
    if ([tapOption isEqualToString:@"1"]) {
        custom = YES;
    }
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    [[AppsaholicSDK sharedManager] trackEvent:ONE_TAP withSubID:uuid notificationType:custom withController:self withSuccess:^(BOOL success, NSString *notificationText, NSNumber *points) {
        if (success) {
            NSLog(@"One tap Call from library successful");
        }
    }];
}
- (IBAction)adsaholicQuiz:(id)sender
{
    [[AppsaholicAds sharedManager] showVideoAd:PLACEMENT_ID withContextController:self];
}

- (void)adCompletedSuccessfully{
    [[AdsaholicSDK sharedManager] presentQuizForAdvertisement:@"1" userId:@"1" error:nil completionHandler:nil];
}

- (void)adServerWaterFallFail{
    UIAlertView *statusAlertView = [[UIAlertView alloc]initWithTitle:@"Perk Points = Cash!" message:@"Perk currently unavailable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [statusAlertView show];
}

- (void)adLoadedSuccessfully{}
- (void)adServerWaterFallStarted{}
- (void)adLoadingFail{}
- (void)adDidAppear{}
- (void)adDidDisappear{}
- (void)adWillDisappear{}
- (void)adClosedWithoutCompletion{}
- (void)adIsTouched{}

@end
