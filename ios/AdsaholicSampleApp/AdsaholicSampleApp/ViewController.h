//
//  ViewController.h
//  AdsaholicSample
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)openPortal:(id)sender;
- (IBAction)login:(id)sender;
- (IBAction)oneTap:(id)sender;
- (IBAction)adsaholicQuiz:(id)sender;

@end

