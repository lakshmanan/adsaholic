#!/bin/sh
set -e

mkdir -p "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"

RESOURCES_TO_COPY=${PODS_ROOT}/resources-to-copy-${TARGETNAME}.txt
> "$RESOURCES_TO_COPY"

install_resource()
{
  case $1 in
    *.storyboard)
      echo "ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile ${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .storyboard`.storyboardc ${PODS_ROOT}/$1 --sdk ${SDKROOT}"
      ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .storyboard`.storyboardc" "${PODS_ROOT}/$1" --sdk "${SDKROOT}"
      ;;
    *.xib)
        echo "ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile ${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .xib`.nib ${PODS_ROOT}/$1 --sdk ${SDKROOT}"
      ibtool --reference-external-strings-file --errors --warnings --notices --output-format human-readable-text --compile "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename \"$1\" .xib`.nib" "${PODS_ROOT}/$1" --sdk "${SDKROOT}"
      ;;
    *.framework)
      echo "mkdir -p ${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      echo "rsync -av ${PODS_ROOT}/$1 ${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      rsync -av "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
      ;;
    *.xcdatamodel)
      echo "xcrun momc \"${PODS_ROOT}/$1\" \"${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1"`.mom\""
      xcrun momc "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodel`.mom"
      ;;
    *.xcdatamodeld)
      echo "xcrun momc \"${PODS_ROOT}/$1\" \"${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodeld`.momd\""
      xcrun momc "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcdatamodeld`.momd"
      ;;
    *.xcmappingmodel)
      echo "xcrun mapc \"${PODS_ROOT}/$1\" \"${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcmappingmodel`.cdm\""
      xcrun mapc "${PODS_ROOT}/$1" "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/`basename "$1" .xcmappingmodel`.cdm"
      ;;
    *.xcassets)
      ;;
    /*)
      echo "$1"
      echo "$1" >> "$RESOURCES_TO_COPY"
      ;;
    *)
      echo "${PODS_ROOT}/$1"
      echo "${PODS_ROOT}/$1" >> "$RESOURCES_TO_COPY"
      ;;
  esac
}
          install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/account.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/arrow.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/back.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/claim_btn_notification1a.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/close_button.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/earn_more_btn_1a2.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/exclaimation.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/icon_faq_notification.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/icon_info_notification.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/icon_refresh.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/info_circle.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/loginToPerk.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/logo_notification.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/logo_perk.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_1.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_10.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_2.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_3.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_4.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_5.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_6.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_7.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_8.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/Perk_Point_9.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/perk_star.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/point_notification.png"
                    install_resource "../../AppsaholicUniversalSDK/AppsaholicFramework/AppsaholicResources/TempUnavailable.png"
          
rsync -avr --copy-links --no-relative --exclude '*/.svn/*' --files-from="$RESOURCES_TO_COPY" / "${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
if [[ "${ACTION}" == "install" ]]; then
  rsync -avr --copy-links --no-relative --exclude '*/.svn/*' --files-from="$RESOURCES_TO_COPY" / "${INSTALL_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
fi
rm -f "$RESOURCES_TO_COPY"

if [[ -n "${WRAPPER_EXTENSION}" ]] && [ "`xcrun --find actool`" ] && [ `find . -name '*.xcassets' | wc -l` -ne 0 ]
then
  case "${TARGETED_DEVICE_FAMILY}" in
    1,2)
      TARGET_DEVICE_ARGS="--target-device ipad --target-device iphone"
      ;;
    1)
      TARGET_DEVICE_ARGS="--target-device iphone"
      ;;
    2)
      TARGET_DEVICE_ARGS="--target-device ipad"
      ;;
    *)
      TARGET_DEVICE_ARGS="--target-device mac"
      ;;
  esac
  find "${PWD}" -name "*.xcassets" -print0 | xargs -0 actool --output-format human-readable-text --notices --warnings --platform "${PLATFORM_NAME}" --minimum-deployment-target "${IPHONEOS_DEPLOYMENT_TARGET}" ${TARGET_DEVICE_ARGS} --compress-pngs --compile "${BUILT_PRODUCTS_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}"
fi
