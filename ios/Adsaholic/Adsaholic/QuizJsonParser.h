//
//  QuizJsonParser.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Adsaholic/AdsaholicEnums.h>

@interface QuizJsonParser : NSObject

+ (NSString*)jsonWithAdvertisement:(AdvertisementId)advertisementId user:(UserId)userId;

@end
