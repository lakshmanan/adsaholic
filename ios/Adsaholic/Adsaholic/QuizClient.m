//
//  QuizClient.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "QuizClient.h"
#import "RESTClient.h"
#import "QuizJsonParser.h"

@implementation QuizClient

+(QuizClient*)sharedInstance
{
    static QuizClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[QuizClient alloc] init];
    });
    return _sharedInstance;
}

- (NSString*)quizHTMLForAdvertisement:(AdvertisementId)advertisementId user:(UserId)userId error:(NSError**)error
{
    [[RESTClient sharedInstance] DoPost:[NSString stringWithFormat:@"%@/quiz",REST_API_URL]
                                   body:[QuizJsonParser jsonWithAdvertisement:advertisementId user:userId]
                                headers:nil
                                  error:error];
    return [RESTClient sharedInstance].responseString;
}
@end
