//
//  Adsaholic.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Adsaholic.
FOUNDATION_EXPORT double AdsaholicVersionNumber;

//! Project version string for Adsaholic.
FOUNDATION_EXPORT const unsigned char AdsaholicVersionString[];

#import <Adsaholic/AdsaholicSDK.h>
#import <Adsaholic/AdsaholicEnums.h>
