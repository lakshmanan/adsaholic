//
//  AdsaholicSDK.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "AdsaholicSDK.h"
#import "AdsaholicEnums.h"
#import "QuizViewController.h"
#import "QuizWebView.h"

NSString *errorDomain = @"com.mozarts.Adsaholic.Error";

@implementation AdsaholicSDK

+(AdsaholicSDK*) sharedManager
{
    static AdsaholicSDK *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AdsaholicSDK alloc] init];
    });
    return sharedInstance;
}

- (BOOL) presentQuizForAdvertisement:(AdvertisementId)advertisementId userId:(UserId)userId error:(NSError**)error completionHandler:(completeBlock)handler
{
    BOOL bStatus = false;
    if(self.appRootViewController)
    {
        if(self.appRootViewController.navigationController)
        {
            QuizViewController* quizViewController = [[QuizViewController alloc] initWithAdvertisement:advertisementId userId:userId];
            [self.appRootViewController.navigationController pushViewController:quizViewController animated:YES];
            
        }
        else
        {
            if(error)
                *error = [NSError errorWithDomain:errorDomain code:AppRootViewControllerIsNotANavigationController userInfo:nil];
        }
    }
    else
    {
        if(error)
            *error = [NSError errorWithDomain:errorDomain code:AppRootViewControllerPropertyNotSet userInfo:nil];
    }
    return bStatus;
}

@end
