//
//  QuizWebView.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizWebView : UIWebView

@property (strong, nonatomic) UIButton *closeButton;
@property (strong, nonatomic) UILabel *loadingLabel;

- (instancetype)init;

- (void)startLoadingAnimation;
- (void)stopLoadingAnimation;

@end
