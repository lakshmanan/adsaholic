//
//  RESTClient.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "RESTClient.h"

@interface RESTClient()

@end

@implementation RESTClient

+(RESTClient *) sharedInstance
{
    static RESTClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[RESTClient alloc] init];
    });
    return _sharedInstance;
}

-(BOOL) DoPost:(NSString *)url body:(NSString *)body headers:(NSDictionary*)headers error:(NSError**)error
{
    return [self Request:url body:body method:@"POST" headers:headers error:error];
}

-(BOOL) DoGet:(NSString *)url headers:(NSDictionary *)headers error:(NSError**)error
{
    return [self Request:url body:@"" method:@"GET" headers:headers error:error];
}

-(BOOL) Request:(NSString *)url body:(NSString *)body method:(NSString *)method headers:(NSDictionary*)headers error:(NSError**)error
{
    // Create Http Request
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:nsUrl cachePolicy:self.cachePolicy timeoutInterval:self.timeout];
    // Set Method
    [urlReq setHTTPMethod:method];
    // Set Body
    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [urlReq setHTTPBody:postData];
    // Set Headers
    for (NSString* key in headers) {
        NSString *value = headers[key];
        [urlReq setValue:value forHTTPHeaderField:key];
    }
    NSLog(@"Request - URL: %s Method: %s\nBody: %s", url.UTF8String, method.UTF8String, body.UTF8String);
    
    // Synchronous Request
    NSHTTPURLResponse  *response;
    NSData *responseData;
    responseData = [NSURLConnection sendSynchronousRequest:urlReq returningResponse:&response error:error];
    
    // Error
    if(*error)
    {
        self.responseString = @"";
        self.responseStatus = @"";
        self.responseCode = 0;
        NSLog(@"Reponse - URL: %s Body: Error Code:%ld Desc:%s", url.UTF8String, (long)(*error).code, (*error).description.UTF8String);
        return false;
    }
    else
    {
        self.responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        self.responseCode = response.statusCode;
        self.responseStatus = [NSHTTPURLResponse localizedStringForStatusCode:response.statusCode];
        NSLog(@"Reponse - URL: %s Body: %s Status:%s Code:%ld", url.UTF8String, _responseString.UTF8String, _responseStatus.UTF8String, (long)_responseCode);
        return true;
    }
}


@end
