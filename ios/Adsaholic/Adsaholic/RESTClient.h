//
//  RESTClient.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <Foundation/Foundation.h>

static const NSString* REST_API_URL = @"http://localhost:3000/";

@interface RESTClient : NSObject

@property (strong, atomic) NSString *responseString;
@property (strong, atomic) NSString *responseStatus;
@property long responseCode;

@property (atomic,readwrite) NSTimeInterval timeout;
@property (atomic,readwrite) NSURLRequestCachePolicy cachePolicy;

+(RESTClient*)sharedInstance;

- (BOOL) DoGet: (NSString*)url headers:(NSDictionary*)headers  error:(NSError**)error;
- (BOOL) DoPost:(NSString*)url body:(NSString*)body headers:(NSDictionary*)headers  error:(NSError**)error;


@end
