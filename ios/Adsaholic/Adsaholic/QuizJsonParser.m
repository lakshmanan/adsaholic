//
//  QuizJsonParser.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "QuizJsonParser.h"

@implementation QuizJsonParser

+ (NSString*)jsonWithAdvertisement:(AdvertisementId)advertisementId user:(UserId)userId
{
    NSString *json;
    if (userId)
        json = [NSString stringWithFormat:@"{\"adid\":\"%ld\",\"userid\":\"%ld\"}",advertisementId, userId];
    else
        json =[NSString stringWithFormat:@"{\"adid\":\"%ld\"}",advertisementId];
    return json;
}

@end
