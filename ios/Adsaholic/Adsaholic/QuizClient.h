//
//  QuizClient.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Adsaholic/AdsaholicEnums.h>

@interface QuizClient : NSObject

+(QuizClient*)sharedInstance;

-(NSString*)quizHTMLForAdvertisement:(AdvertisementId)advertisementId user:(UserId)userId error:(NSError**)error;

@end
