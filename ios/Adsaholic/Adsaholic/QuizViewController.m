//
//  QuizViewController.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "QuizViewController.h"
#import "QuizClient.h"

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor grayColor]];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.quizWebView = [[UIWebView alloc] initWithFrame:self.view.frame];
    [self.quizWebView setBackgroundColor:[UIColor grayColor]];
    [self.quizWebView setScalesPageToFit:NO];
    [self.view addSubview:self.quizWebView];
    [self addWebViewConstraints];
    [self makeRequest];
}

-(instancetype)initWithAdvertisement:(AdvertisementId)advertisementId userId:(UserId)userId
{
    self = [super init];
    if(self)
    {
        self.advertisementId = advertisementId;
        self.userId = userId;
     }
    return self;
}

-(void)addWebViewConstraints
{
//    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSArray* constraints = [[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[webView]-0-|" options:0 metrics:nil views:@{@"webView" : self.quizWebView}] arrayByAddingObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[webView]-0-|" options:0 metrics:nil views:@{@"webView" : self.quizWebView}]];
    
    [self.view addConstraints:constraints];
}

- (void)makeRequest
{
    NSString *urlString = [NSString stringWithFormat:@"http://localhost:3000/takequiz?adid=%@",self.advertisementId];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [self.quizWebView loadRequest:request];
}


@end
