//
//  AdsaholicSDK.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

#import <Adsaholic/AdsaholicEnums.h>

typedef void (^completeBlock) (BOOL success, NSString *status);

@interface AdsaholicSDK : NSObject

/**
 A must property to assign to work SDK. SDK needs to know application controller from where it needs to present quiz for the advertisement.
 */

@property (strong, nonatomic) UIViewController *appRootViewController;

+(AdsaholicSDK*) sharedManager;

- (BOOL) presentQuizForAdvertisement:(AdvertisementId)advertisementId userId:(UserId)userId error:(NSError**)error completionHandler:(completeBlock)handler;

@end
