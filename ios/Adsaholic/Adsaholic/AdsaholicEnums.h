//
//  AdsaholicEnums.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#ifndef AdsaholicEnums_h
#define AdsaholicEnums_h

typedef enum
{
    AppRootViewControllerPropertyNotSet = 0,
    AppRootViewControllerIsNotANavigationController = 1 << 0
} AdsaholicErrorCode;

typedef NSString* AdvertisementId;
typedef NSString* UserId;


#endif /* AdsaholicEnums_h */
