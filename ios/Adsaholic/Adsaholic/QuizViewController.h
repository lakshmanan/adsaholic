//
//  QuizViewController.h
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Adsaholic/AdsaholicEnums.h>
#import "QuizWebView.h"

@interface QuizViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *quizWebView;
@property (nonatomic) AdvertisementId advertisementId;
@property (nonatomic) UserId userId;

-(instancetype)initWithAdvertisement:(AdvertisementId)advertisementId userId:(UserId)userId;

@end
