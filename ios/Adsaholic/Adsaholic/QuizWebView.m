//
//  QuizWebView.m
//  Adsaholic
//
//  Created by Saravanakumar V on 19/12/15.
//  Copyright © 2015 mozarts. All rights reserved.
//

#import "QuizWebView.h"

@implementation QuizWebView

- (instancetype)init
{
    self = [super init];
    if(self)
    {
//        [self addSubview:self.closeButton];
    }
    return  self;
}
- (void)startLoadingAnimation
{
    
}
- (void)stopLoadingAnimation
{
    
}

-(UIButton*)_closeButton
{
    if(!_closeButton)
    {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton addTarget:self action:@selector(_closeButton) forControlEvents:UIControlEventTouchUpInside];
        [_closeButton setImage:[UIImage imageNamed:@"close_button"] forState: UIControlStateNormal];
//        _closeButton.frame = CGRectMake(claimButton.frame.origin.x + claimButton.frame.size.width + 5, claimButton.frame.origin.y, 35, 35);
    }
    return _closeButton;
}

-(UILabel*)loadingLabel
{
    if(_loadingLabel)
    {
        _loadingLabel = [[UILabel alloc] init];
    }
    return _loadingLabel;
}

@end
