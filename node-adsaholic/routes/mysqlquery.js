var express = require('express');
var router = express.Router();
var ut = require('../util');

router.all('/qas', function(req, res, next) {
  var adid = undefined;
  if (req.body.adid) {
    adid = req.body.adid;
  }
  else if (req.query.adid) {
    adid = req.query.adid
  }
  if (adid != undefined) {
    ut.getQAS(adid, function (rows) {
      res.send(rows);
    });
  }
});

router.post('/qas/add', function(req, res, next) {
  var rec = req.body;
  ut.addQA(rec, function (r) {
      res.send(r);
  });
});

router.post('/qas/delete', function(req, res, next) {
  var qaid = req.body.qaid;
  ut.deleteQA(qaid, function (r) {
    res.send(r);
  });
});

router.post('/qas/update', function(req, res, next) {
  var rec = req.body;
  ut.updateQA(rec, function (r) {
    res.send(r);
  });
});

module.exports = router;