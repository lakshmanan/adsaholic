var express = require('express');
var router = express.Router();

var ut = require('../util');

/* GET home page. */
router.all('/takequiz', function(req, res, next) {
  var adid = undefined;
  if (req.body.adid) {
    adid = req.body.adid;
    console.log('Picking body')
  }
  else if (req.query.adid) {
    adid = req.query.adid
    console.log('Picking query')
  }
  if (adid != undefined) {
    ut.getQAS(adid, function(rows) {
      res.render('takequiz', {r: rows });
    });
  }
  else
    res.render('error', {error: {status: "Invalid Ad ID"}})
});


/* GET home page. */
router.all('/', function(req, res, next) {
  var adid = undefined;
  if (req.body.adid) {
    adid = req.body.adid;
    console.log('Picking body')
  }
  else if (req.query.adid) {
    adid = req.query.adid
    console.log('Picking query')
  }
  if (adid != undefined) {
    res.render('setquiz', {adid: adid });
  }
  else
    res.render('error', {error: {status: "Invalid Ad ID"}})
});

module.exports = router;
