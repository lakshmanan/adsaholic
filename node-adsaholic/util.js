/**
 * Created by lan on 12/19/15.
 */

var mysql = require('mysql');
var config = require('./config');

var con = mysql.createConnection(config.db);

con.connect(function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log('Connected to MySQL successfully!!');
    }
})

function getQAS(adid, callback) {
    con.query("SELECT * FROM qas WHERE adid = ?;", [adid], function(err, rows) {
        if (err) throw err;
        callback(rows);
    })
}

function addQA(rec, callback) {
    con.query("INSERT INTO qas SET ?", rec, function(err, res) {
        if (err) throw err;
        callback(res);
    })
}

function updateQA(rec, callback) {
    con.query("UPDATE qas SET ? WHERE qaid = ?", [rec, rec.qaid],  function(err, res){
        if(err) throw err;
        callback(res);
    })
}

function deleteQA(qaid, callback) {
    con.query("DELETE FROM qas WHERE qaid = ?", qaid,  function(err, res){
        if(err) throw err;
        callback(res);
    })
}

module.exports = {
    getQAS: getQAS,
    addQA: addQA,
    deleteQA:deleteQA,
    updateQA: updateQA
};


