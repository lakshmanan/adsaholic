/**
 * Created by lan on 12/19/15.
 */

// indexing starts from 0
var totalQ;
var currQ;
var correctA = [];

$( document ).ready(function() {
    totalQ = Number($("#total").text());
    $("#submit").click(function (){onSubmit();})
    $("#result").css('display', 'none');
    showQ(0);
});

function notifyCorrectness(correct) {
    var color = correct? "#090" : "#900";
    $("body").animate({
        'background-color': color
    }, 500).animate({
        'background-color': "#fff"
    }, 500);
}

function showQ(q) {
    currQ = q;
    $('#curr').text(currQ+1);
    console.log( $('.question').length)
    $('.question').css('display', 'none');
    $('.question').eq(currQ).show();
}

function getSelected() {
    var selected = $("[name=ans" + currQ + "]:checked").val();
    return selected;
}

function onSubmit() {
    var selected = getSelected();
    if(selected == undefined)
        return;
    var correct = selected == $("[name=a" + currQ + "]").val();
    notifyCorrectness(correct);
    correctA[currQ] = correct;

    if (++currQ  < totalQ) {
        showQ(currQ);
    } else {
        showResult();
    }
}

function showResult() {
    var totalPoints = 0;
    for (var x = 0; x < correctA.length; x++) {
        if (correctA[x]) {
            totalPoints += Number($("[name=points" + x + "]").val());
        }
    }
    $("#quiz").css('display', 'none');
    $("#p").text(totalPoints);
    $("#result").show();
}

