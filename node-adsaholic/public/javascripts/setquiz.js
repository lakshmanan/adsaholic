/**
 * Created by lan on 12/20/15.
 */

var adid;
//var server = 'http://node-adsaholic-dev.elasticbeanstalk.com:3000';
var server = 'http://localhost:3000';

$( document ).ready(function() {
    adid = ($("[name=adid]").val());
    loadRows();
    $("#addNewQ").click(function() {
        submitNewRow();
    })
});

function deleteRow(r) {
    var tr = $(r).closest('tr');
    var qaid = $(tr).find("[name=qaid]").val();
    $.ajax({
        url: server + '/query/qas/delete',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({"qaid": qaid})
    }).done(function(r) {
        console.log("Deleted")
        console.log(r)
        $(tr).remove();
    })
}

function updateRow(r) {
    var tr = $(r).closest('tr');
    var qaid = $(tr).find("[name=qaid]").val();
    var q = $(tr).find("[name=q]").val();
    var c = $(tr).find("[name=c]").val();
    var a = $(tr).find("[name=a]").val();
    var p = $(tr).find("[name=p]").val();

    $.ajax({
        url: server + '/query/qas/update',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({"qaid":qaid, "q":q, "choices":c, "a":a, "points":p})
    }).done(function(r) {
        console.log("Updated")
        console.log(r)
    })
}


function submitNewRow() {
    var q = $("[name=newQ]").val();
    var c = $("[name=newC]").val();
    var a = $("[name=newA]").val();
    var p = $("[name=newP]").val();
    $.ajax({
        url: server + '/query/qas/add',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({"adid":adid, "q":q, "choices":c, "a":a, "points":p})
    }).done(function(r){
        var str =
            '<tr><input type="hidden" value="' + r.insertId + '" name="qaid">'
            +   '<td><input name="q" value="' + q + '"></td>'
            +   '<td><input name="c" value="' + c + '"></td>'
            +   '<td><input name="a" value="' + a + '"></td>'
            +   '<td><input name="p" value="' + p + '"></td>'
            +   '<td><input type="button" name="update" value="Update" onclick="updateRow(this)"><input type="button" name="delete" value="Delete" onclick="deleteRow(this)"></td>'
            + '</tr>'
        $('tbody').append(str);
    })
}

function loadRows() {
    $.ajax({
        url: server + '/query/qas?adid=' + adid,
        method: 'GET'
    }).done(function( rows ) {
        $(rows).each(function(i, r) {
            var str =
                '<tr><input type="hidden" value="' + r.qaid + '" name="qaid">'
                +   '<td><input name="q" value="' + r.q + '"></td>'
                +   '<td><input name="c" value="' + r.choices + '"></td>'
                +   '<td><input name="a" value="' + r.a + '"></td>'
                +   '<td><input name="p" value="' + r.points + '"></td>'
                +   '<td><input type="button" name="update" value="Update" onclick="updateRow(this)"><input type="button" name="delete" value="Delete" onclick="deleteRow(this)"></td>'
                + '</tr>'
            $('tbody').append(str);
        })
    });
}