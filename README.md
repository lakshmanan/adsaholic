# "Ads"aholic #

**Adsaholic is a framework/SDK extended on top of Appsaholic by Perk, that lets users earn more rewards by taking up a quiz, based on the last-watched ad, that is set up by the ad publisher.**

The main objective is to reward users not only by displaying ads, but reward them more if they remember what the ad is about. This will increase user memory about the product being advertised, thereby giving the advertisers an opportunity to pitch in more sale.

The idea is to "Pop a quiz" or "Ask feedback"  about the last shown ad, answering which will earn the user more perk points. The current protoype implementation is quiz-based interaction, although it is built in such that it can be extended to feedback/any other user interaction.

More ad views can be achieved with "Watch Again and Answer" feature. If the user answers the quiz incorrectly, he/she is presented with the "Watch Again and Answer" option, where in the user can watch the ad again and retake the quiz to make up for his missed perk points. This option can be presented as many times as the user wants.


### Demo Notes ###
* Please open the xcode workspace present Adsaholic/ios/Adsaholic.xcworkspace in order to run the sample app.
* Sample app may not build when open from the project.pbxproj file.
* iOS project is not code signed and hence can be run in simulator only.
* From node-adsaholic, run **node main.js** to start REST server
* Visit http://<machine_where_nodejs_runs>:3000/adid=1 to view the Advertising Partner's Portal, which can be used to modify the question set for ad whose id is 1. 
* Adsaholic Quiz presents a video ad, end of which a quiz view is shown.
* Quiz view is shown at the successful completion of any Ad component.

### Components ###

* iOS Adsaholic SDK
* iOS Adsaholic Sample App
* REST API powered by Node.js
* Advertiser portal
* Database Model powered by MySQL


#### iOS Adsaholic SDK ####
iOS SDK is a framework that will shipped with the Appsaholic SDK. The design is almost similar to Appsaholic SDK with a singleton interface accessible like,
```
[AdsaholicSDK sharedManager]
```
In order to install the framework to App's project, the programmer has to simply drag drop the framework into *Embedded Frameworks* tab.
The following interface will present a quiz and it should be invoked upon completion of an advertisement,
```
- (BOOL) presentQuizForAdvertisement:(AdvertisementId)advertisementId userId:(UserId)userId error:(NSError**)error completionHandler:(completeBlock)handler
```

*Repository path - Adsaholic/ios/Adsaholic/Adsaholic.xcodeproj*
#### iOS Adsaholic Sample App ####
Sample App demonstrates the usage of Adsaholic SDK along with few Appsaholic SDK features.

*Repository path - "Adsaholic/ios/AdsaholicSampleApp/AdsaholicSampleApp.xcodeproj*

#### REST API ####
The REST API is being handled by Nodejs. This instance serves the iOS SDK's REST client and WebView. 

*Repository path - Adsaholic/node-adsaholic*

#### Advertiser portal ####
This portal is provided to the advertisers to enter quiz details about their ad.

*Repository path - Adsaholic/*
#### Database Model ####
Backend database is served by a mysql-server hosted on Amazon EC2. 

*Repository path - Adsaholic/mysql-model*

### Contact ###

Saravanakumar V - 9620760718
Lakshmanan N - 8130191183